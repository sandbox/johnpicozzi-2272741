<?php
/**
 * @file
 * This file holds the functions for the Bloomerang admin settings.
 */

/**
 * Menu callback; displays the Bloomerang module settings page.
 *
 * @see system_settings_form()
 */
function bloomerang_admin_settings() {
  $form = array();

  // Key.
  $form['bloomerang_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Bloomerang Key'),
    '#description' => t('Enter Your Bloomerang API key'),
    '#default_value' => variable_get('bloomerang_key', ''),
  );
  $form['#submit'][] = 'bloomerang_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Submit handler for system settings form.
 */
function bloomerang_admin_settings_submit($form, &$form_state) {
  drupal_set_message("If you haven't already, please go to the <a href=\"/admin/structure/block\">block administration page</a> to set position and visibility of Bloomerang blocks.");
}
