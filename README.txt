Bloomerang
=======================

https://drupal.org/sandbox/johnpicozzi/2272741

What Is This?
-------------

The Bloomerang module allows basic integration with the Bloomerang Donor
Database system (https://bloomerang.co). This module creates a block for
each Bloomerang widget type, allowing administrators to place forms in any
configured region on your site.

To use this module you will need a Bloomerang account and your Public API key.


How To Use Bloomerang
-----------------------

There are three steps to use this project:

1. Enable the module

2. Navigate to /admin/config/services/bloomerang and enter your Public API Key

3. Navigate to the block administration page and place the Bloomerang blocks.


How To Install The Module
-------------------------

1. Install Bloomerang (unpacking it to your Drupal
/sites/all/modules directory if you're installing by hand, for example).

2. Enable Bloomerang module in Admin > Modules.


If you find a problem, incorrect comment, obsolete or improper code or such,
please search for an issue about it at https://drupal.org/project/issues/2272741
If there isn't already an issue for it, please create a new one.
